package com.tsjgroup.justjava;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.String;

public class MainActivity extends AppCompatActivity {
    int quantity = 0;
    int coffeePrice = 5;
    int whippedCreamPrice = 1;
    int chocolatePrice = 2;
    String address = "frozenvip@gmail.com";
    //String Subject = getString(R.string.subject) + onNameEditText();
    //String Subject = "Coffees";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        displayMessage(createOrderSummary(onNameEditText()));
    }

    public void sendOrderEmail(View view) {
        sendEmail(createOrderSummary(onNameEditText()));
    }

    /**
     * This method is called when the plus button is clicked.
     */
    public void increment(View view) {

        if (quantity == 100) {
            Toast.makeText(this, getString(R.string.increment), Toast.LENGTH_SHORT).show();
            return;
        }
        quantity++;
        displayQuantity(quantity);
    }

    /**
     * This method is called when the minus button is clicked.
     */
    public void decrement(View view) {
        if (quantity > 1) {
            quantity--;
            displayQuantity(quantity);
        }
        else if (quantity == 0) {
            Toast.makeText(this, getString(R.string.decrement_0), Toast.LENGTH_SHORT).show();
            return;
        } else {
            Toast.makeText(this, getString(R.string.decrement_1), Toast.LENGTH_SHORT).show();
            return;
        }
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayQuantity(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

    /**
     * This method displays the given text on the screen.
     */
    private void displayMessage(String message) {
        TextView orderSummaryTextView = (TextView) findViewById(R.id.order_summary_text_view);
        orderSummaryTextView.setText(message);
    }

    /**
     * Calculates the price of the order based on the current quantity.
     *
     * @return the price
     */
    private int calculatePrice() {
        int price = coffeePrice;
        if (onWcCheckBoxClicked() == true) {
            price += whippedCreamPrice;
        }
        if (onChocoCheckBoxClicked() == true) {
            price += chocolatePrice;
        }
        price *= quantity;
        return price;
    }

    private String createOrderSummary(String name) {
        int price = calculatePrice();
        String priceMessage = getString(R.string.name_order_sum) + " " + name;
        priceMessage += "\n" + getString(R.string.add_whipped_cream) + " " + onWcCheckBoxClicked();
        priceMessage += "\n" + getString(R.string.add_chocolate) + " " + onChocoCheckBoxClicked();
        priceMessage += "\n" + getString(R.string.quantity) + ": " + quantity;
        priceMessage += "\n" + getString(R.string.total_order_sum) + price;
        priceMessage += "\n" + getString(R.string.thank_you);
        return priceMessage;
    }

    public boolean onWcCheckBoxClicked() {
        /** Получаем флажок*/
        CheckBox wcreamCheckBox = (CheckBox) findViewById(R.id.wcream_checkbox);
        /**Получаем, отмечен ли данный флажок*/
        boolean isWcCheckBox = wcreamCheckBox.isChecked();
        return isWcCheckBox;
    }

    public boolean onChocoCheckBoxClicked() {
        /** Получаем флажок*/
        CheckBox chocolateCheckBox = (CheckBox) findViewById(R.id.chocolate_checkbox);
        /**Получаем, отмечен ли данный флажок*/
        boolean isChocoCheckBox = chocolateCheckBox.isChecked();
        return isChocoCheckBox;
    }

    private String onNameEditText() {
        /** Получаем флажок*/
        EditText nameEditText = (EditText) findViewById(R.id.name_edit_text);
        /**Получаем, отмечен ли данный флажок*/
        String nameEditTexts = nameEditText.getText().toString();
        return nameEditTexts;
    }

    public void sendEmail (String textBody)
    {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + address)); // only email apps should handle this
        //intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subject) + ":8 " + onNameEditText());
        intent.putExtra(Intent.EXTRA_TEXT, textBody);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

}
